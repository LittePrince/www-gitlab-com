---
layout: handbook-page-toc
title: Expenses
---

## Introduction

Welcome to the Expenses page!  You should be able to find answers to most of your questions here.  If you can't find what you are looking for, then:
- **Chat channel**: #expense-reporting-inquiries
- **Email**: expenses@gitlab.com

All team members will have access to Expensify within 2 days from their hire date.  If you didn't receive an email from Expensify for your access, then please contact expenses@gitlab.com. Expense reports are to be submitted once a month, at least. Additional information on getting started with Expensify and creating/submitting expense reports can be found [here.](https://docs.expensify.com/using-expensify-day-to-day/using-expensify-as-an-expense-submitter/report-actions-create-submit-and-close)

The procedure by which reimbursable expenses are processed varies and is dependent on contributor legal status (e.g. independent contractor, employee) and subsidiary assignment (BV, Canada Corp, Federal, Inc, GmbH, LTD, PTY). Check with Payroll if you are unsure about either of these.

For information regarding the company expense policy, check out the section of our team handbook on [Spending Company Money](/handbook/spending-company-money). Managers and Payroll team will review the expenses for compliance with the company travel policy.  The CEO will review selected escalations at least annually.

Team members should also consider the terms and conditions of their respective contractor agreements, when submitting invoices to the company.

Team members in a US policy will be automatically reimbursed through Expensify after their report is "final approved" within 7 business days by Payroll team. For all other team members, please see the reimbursement process below based on your location or employment status


### Setting up a home office for the first time?

Take inspiration from our [all-remote page covering key considerations for a comfortable, ergonomic workspace](/company/culture/all-remote/workspace/). You can also consult the `#questions` and `#remote` channels in Slack for recommendations from other GitLab team members.


### Not sure what to buy?

Look at our [equipment examples page](/handbook/spending-company-money/equipment-examples) to see some of the items that other GitLab team members have purchased, and please consider adding to the list if there's something you'd like to share.

### Coworking or external office  space
If working from home is not practical you may submit for reimbursement for the cost of a co-working space. This can include non-traditional spaces that require a recurring (full-time monthly) membership as long as you average at least ~4 working days per month at the space. If flexible membership options exist in the form of daily passes or hourly packages, then these can be expensed as well, as long as the prorated cost per month does not exceed that of a recurring membership subscription. For instance, if both the monthly subscription and a hypothetical 10-day pass is $200 USD, then you can only expense one such pass each month.

Any agreement must be between the team member and the co-working space (i.e. GitLab will not sign or appear on the agreement). All expenses must be submitted through the normal [travel and expense reimbursement policy](/handbook/finance/accounting/#reimbursable-expenses). The Company will not be responsible for any expense that relates to office space subsequent to the termination of service between GitLab and the team member.

### Work-related online courses and professional development certifications

GitLab team members are allotted [$500 USD](/handbook/total-rewards/compensation/#exchange-rates) per fiscal year to spend on one or multiple training courses. Reimbursement past the [$500 USD](/handbook/total-rewards/compensation/#exchange-rates) total allotment requires manager approval.
1. The company will pay for all courses related to learning how to code (for example [Learning Rails on Codecademy](https://www.codecademy.com/learn/learn-rails)), and you may also allocate work time to take courses that interest you. If you are new to development, we encourage you to learn Git through GitLab, and feel free to ask any questions in the #git-help Slack channel.
1. Work-related conferences, including travel, lodging, and meals. If total costs exceed [$500 USD](/handbook/total-rewards/compensation/#exchange-rates), reimbursement requires prior approval from your manager.
	* Before scheduling any travel or time off to attend a conference a team member should review the request with their manager. The manager will approve the request if the conference is work-related and the timing doesn't interfere with GitLab deliverables. After  manager approval the team member can schedule travel and will be reimbursed for related expenses.
	* We encourage people to be speakers in conferences. More information for people interested in speaking can be found on our [Corporate Marketing](/handbook/marketing/corporate-marketing/#speakers) page.
	* We suggest to the attendees bring and share a post or document about the news and interesting items that can bring value to our environment.

### Year-end Holiday Party Budget

GitLab grants [$100 USD](/handbook/total-rewards/compensation/#exchange-rates) per GitLab team member for a holiday in December We encourage GitLab team members to self organize holiday parties with those close by, but meeting up with GitLab team members is not a requirement as it is understood that timing and location of holiday parties will play a factor in ability to attend.

For those who do not meet up with GitLab team members, you may expense up to $100 USD December or January for a holiday celebration of your choosing.

If a group of team members, Department or Division so choose, they are welcome to donate a portion of their per person Holiday budget to Charity.

## Travel and Expense Guidelines

### Travel

1. For travel to other team members please see our [visiting grant](/handbook/incentives/#visiting-grant).
1. If you are taller than 1.95m or 6'5", you can upgrade to Economy Plus. There is no dollar restriction on this since it will be hard to fit in economy with that height.
1. For flights longer than 8 hours, you can expense:
   * Up to the first [$300 USD](/handbook/total-rewards/compensation/#exchange-rates) for an upgrade to Business Class on flights longer than 8 hours if you are taller than 1.95m or 6'5".
    * Up to the first [$100 USD](/handbook/total-rewards/compensation/#exchange-rates) for an upgrade to Economy Plus (no height restriction) on flights longer than 8 hours.
1. GitLab does not cover expenses for Significant others or family members for travel or immigration. This includes travel and visas for GitLab events.
	* There are other things that [the company will not reimburse](/handbook/finance/accounting/#8-employee-reimbursements---expensify), such as dog boarding.

For additional Company Travel related questions, please refer to our [Travel Handbook](/handbook/travel/#booking-travel-through-tripactions-) regarding booking travel through TripActions.

### Spend reduction

When reducing spend, we'll not take the easy route of (temporarily) reducing discretionary spending.
Discretionary spending includes expenses like travel, conferences, gifts, bonuses, merit pay increases and summits.
By reducing in these areas we put ourselves at risk of [increasing voluntary turnover among the people we need most](https://steveblank.com/2009/12/21/the-elves-leave-middle-earth-–-soda’s-are-no-longer-free/).
Discretionary spending is always subject to questioning, we are frugal and all spending needs to contribute to our goals.
But we should not make cuts in reaction to the need to reduce spend; that would create a mediocre company with mediocre team members.
Instead, we should do the hard work of identifying positions and costs that are not contributing to our goals.
Even if this causes a bit more disruption in the short term, it will help us ensure we stay a great place to work for the people who are here.

### Renting Cars

#### US and Canada

##### Third Party Liability

Purchase the liability insurance that is excess of the standard inclusion of State minimum coverage in the rental agreement at the rental agency. GitLab’s insurance policy provides liability insurance for rental cars while conducting company business, but it may be excess over any underlying liability coverage through the driver or credit card company used to purchase the rental.

Purchase the liability offered at the rental counter if there are foreign employees renting autos in the US or Canada. While workers' compensation would protect an injured US employee, other passengers may have the right to sue. To ensure that GitLab has protection when a foreign employee invites another person into the car we recommend the purchase of this insurance when offered at the rental counter.

##### Physical Damage - Collision Damage Waiver

**Do Not** purchase the Collision Damage Waiver offered at the rental counter. GitLab purchases coverage for damage to rented vehicles.
 If travel to Mexico is required, **purchase** the liability insurance for Mexico offered at the rental counter. You should verify that the rental agreement clearly states that the vehicle may be driven into Mexico and liability coverage will apply.

#### Countries other than the US and Canada

##### Third Party Liability

Purchase the liability insurance offered at the rental counter when traveling outside the US and Canada. Automobile Bodily Injury and Property Damage Liability insurance are required by law in almost every country. Please verify this coverage is included with the rental agreement.

##### Physical Damage - Collision Damage Waiver

Purchase the Collision Damage Waiver or Physical Damage Coverage offered by the rental agency when traveling outside the US and Canada.

In the event of an accident resulting in damage to the rental car, the foreign rental agency will charge the credit card used to make the reservation with an estimated amount of repair costs if insurance is not purchased. If this happens, GitLab does not purchase Foreign Corporate Hired Auto Physical Damage Coverage to reimburse for damages.


### Virtual Meal with GitLab Team member(s)

Due to the suspension of the [Visiting Grant](/handbook/incentives/#visiting-grant), we are temporarily replacing it with the Virtual Meal with [GitLab Team Member(s)](/handbook/communication/#top-misused-terms):
*  from now until April 30, 2020, you can submit up to 2 meals
*  up to $25 per meal (not limited to pizza)
*  submit it under Meals provided by Company
*  add "Virtual Meal with GitLab Team Member" on the description line
*  list the attendee and a screenshot of the virtual party to the expense report. The screenshot is the receipt for this purpose.

### Something else?

No problem, and consider adding it to this list if others can benefit as well.

1. Customer/Partner Facing Events
    * Gala/Black Tie Events: Tuxedo or Gown Rental, $150-$225 USD per event.
    * The event must be customer specific and the invitation must state black tie only.

## Expense Reimbursement

1. Effective 2019-07-01, all expense reports must be submitted to your manager for approval prior to being sent to Finance for payment.
1. If you are a team member from Nigeria, please submit your expense in your salary invoice (a template can be found [here](/handbook/finance/#invoice-template-and-where-to-send)) with receipts attached to <payroll@gitlab.com>.  Please note, this is a temporary solution while we are transition over to a PEO.
1. If you are a team member and incurred an expense charged in a currency different from the one you use to submit your invoices, use the conversion rates specified in the [global compensation section of the handbook](/handbook/total-rewards/compensation/#exchange-rates). If the expense currency doesn’t exist in that list, refer to the conversion rates in [oanda](https://www.oanda.com/currency/converter/). Make sure to set the expense date in the currency converter form.
1. GitLab uses Expensify to facilitate the reimbursement of your expenses. As part of onboarding you will receive an invitation by email to join GitLab's account. Please set up your account by following the instructions in the invitation.
	* If you are a team member in Spain or France, please submit your expenses through Safeguard in-house expense reimbursement management system and also submit them through Expensify.  Payroll will review, approve, and send the approval of your expense reports in Expensify to your gitlab email address.  You will need to forward the approval email to Safeguard enable for them to process your expense reimbursement via payroll.
1. Please make an effort to combine multiple expenses into a single report as it will save the company money by avoiding excessive Expensify fees.
	* If you are new to Expensify and would like a brief review, please see [Getting Started](http://help.expensify.com/getting-started/)
	* For step by step instructions on creating, submitting, and closing a report please see [Create, Submit, Close](https://docs.expensify.com/en/articles/2921-report-actions-create-submit-and-close)
	* For US team members, the approved expense amount will be deposited into your account a few days after the report has been approved by payroll.
	* For Australia, Belgium, Germany, India, and Netherlands, AP will process the approved report on Friday once payroll approved the report.  The payment will be deposited into your account no later than three business days the following week.
	* For all team members being pay by Safeguard, iiPay, or Vistra, the approved expense amount will be deposited in your account with your monthly salary.
1. If you are a team member with a company credit card, your company credit card charges will automatically be fed to a new Expensify report each month. Please attach receipts for these expenses (per the Expense Policy, see below) within 5 business days after the end of the month. These amounts will not be reimbursed to you but Expensify provides a platform for documenting your charges correctly.

### Reimbursement process and timeline:

##### SafeGuard
Team members who are employed through SafeGuard must submit their expense for reimbursement through Expensify.  All expense reports must be submitted and approved by manager by the 8th of each month to include in the current month payment.

Team members in France, Italy, and Spain must submit their expenses through:
*  Expensify
*  Safeguard in-house expense reimbursement
*  GitLab payroll send the expense approval to Safeguard after the team member's manager approved the report
*  Team members send the original receipts to Safeguard


##### Global Upside

* Team members must submit their expenses through Expensify, and Payroll will approve for reimbursement within 5 business days after the approval from the manager.  The reimbursement is through GitLab AP.

##### iiPay

* All Individual contractors or C2C, with exception of Hungary, Nigeria, South Africa, and Switzerland, will be reimbursed by iiPay by the 22nd of each month.  All expense reports must be approved by manager by the 8th of each month to be include in the current month payment.  For contractor with C2C status, be sure to contact Payroll team via email at nonuspayroll@gitlab.com if you need to set up a separate bank for your expense reimbursement.

##### Legal entities

* Expense reports for GitLab Ltd (UK) must be approved by the manager on or before the 14th of each month enable for it to include in the current month payroll.
* Expense reports for GitLab Canada Corp must be approved by the manager before the 1st day of each payroll period.  Please see [Payroll Calendar](https://docs.google.com/spreadsheets/d/1ECkI_Z8R82j1eipJEEybXjO-EDtzw4TuhJPOnHypDho/edit#gid=0) for the payroll cut off date.
* Expense reports for GitLab BV (Belgium and Netherlands), GmbH (Germany), PTY Ltd (Australia and New Zealand) are reimbursed via GitLab AP within 10 business days from the approval date by their manager.
* Expense reports for GitLab Inc, GitLab Inc Billable, and GitLab Federal reimbursed via Expensify, and AP will final approved the report within 5 business days after the approval from their manager.

##### Hungary, Nigeria, South Africa

* Please include your expenses along with receipts on your monthly salary invoice.

##### Poland, Russia, Ukraine

* Expense reports must be approved by the manager on or before the 8th of each month enable for it to be included in the current month payroll
* GitLab Payroll will send the approved expense amount to CXC EMEA payroll included with the monthly salary
* Team members must include the approved expense amount on their monthly invoice

## Expense Policy

1. Max Expense Amount - [$5,000 USD](/handbook/total-rewards/compensation/#exchange-rates) - NOTE - If you are a corporate credit card holder, please refer to the [corporate credit card policy section](https://about.gitlab.com/handbook/finance/accounting/#credit-card-use-policy) for those specific instructions and thresholds.
1. Receipt Required Amount - [$25 USD](/handbook/total-rewards/compensation/#exchange-rates)
1. Expenses must be submitted within 90 days of purchase.

## Advance

These instructions apply if a team member is unable to purchase items, for whatever reason.

1. New team member will make a list of requested items and prices, noting if they are out of the budget range listed on this page (if applicable), and send to their manager for approval. We ask that only one list be sent, versus multiple lists.
1. The team member's manager will send the approved (or edited) list to Accounting (nonuspayroll@domain.com OR uspayroll@domain.com, and CC ap@domain.com) for final approval and dispensation.
1. Once approved, Payroll will send the team member an invoice template to fill with the approved items, prices and the team member's bank information.
1. The approved final amount will be sent to the team member's bank and they can then purchase their approved items.

## Approving Expense Reports

1. Expensify will send a notification email when a team member submitted an expense report
    * Click on the report name in the body of the email
    * Review each expense for the correct amount of the receipt and the report
    * Check for customers or project name if applicable under Tag
    * We required a receipt for any expense greater than $25 (except for Billable policy)
    * Select [Approve and Forward] option and Expensify pre-populated the email address.  Note, Expensify is updating their coding to address a small glitch in this field.  If it is empty, please send it to **Montpac** (gitlab-expensify-mp@montpac.com)
    * **Important** - please do not use [Final Approval] because Expensify will not send the email notification for payment approval and it will delay the reimbursement process
    * Manager can delegate the approval process during PTO:
        *  Settings
        *  Your Account
        *  Vacation Delegate
        *  Enter the email address of the backup approval
    * All expense question(s) can be addressed via expenses@gitlab.com or in the #Finance and #expense-reporting-inquires
Slack channel

1.  **Expenses Reports approval deadline**
    * Australia, Germany, India, Netherlands, United States - as soon as possible
    * United Kingdom - all expense reports must be approved by manager no later than the 14th of each month.  Team members - please be sure to submit your report(s) couple days before the due date so your manager has enough time for approval.
    * All non-US contractors - all expense reports must be approved by manager no later than the 8th of each month. Team members - please be sure to submit your report(s) couple days before the due date.

## Non-reimbursable Expenses:

Examples of things we have not reimbursed:
1. Costume for end of summit party.
1. Boarding expense for dog while traveling.
1. Headphones costing $800 which were found to be in excess of our standard equipment guidelines.
1. Batteries for smoke detector.
1. Meals during the Contribute when team members opt out of the company provided meal option.
1. Cellphones and accessories.
1. Travel related expenses for family members of GitLab employees
1. Fitness equipments (treadmill, etc..) and gym membership
1. Meals from the co-working day(s)

In accordance with [Reimbursable Expense guidelines](/handbook/finance/accounting/#reimbursable-expenses), independent contractors should note which expenses are Contribute related on their invoices, prior to submitting to the company.




