---
layout: handbook-page-toc
title: Building High Performing Teams
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

One of the greatest challenges for leaders is to build and engage high performing teams. This page explores what it means to have a high performing team at GitLab. It will also explore a tactical roadmap called the Drexler-Sibbet Team Performance Model that managers can reference to build high performing teams. 

## What Makes a High Performing Team at GitLab

Through various interviews with executive leadership and managers at GitLab, we have identified a series of skills, behaviors, and attributes of high performing teams at GitLab. Many of these points are in alignment with our values. To operate as a team in a remote enviornment, trust needs to be at the center of the formation of the team. Additional skills, behaviors, and attributes of high performing teams include: 

- Handbook first approach
- Shared desire to grow others
- High Level of self-awareness
- Living our values
- Strong sense of ownership by individual team members
- Ego's are put into check
- Ability to see view points from different perspectives
- Bias towards action
- Asyncrhonous communication practices
- Iteration, breaking down complex information into digestible parts 
- Trust based relationships where everyone is open to provide feedback
- Led by Managers who are willing to give their time to the team to guide and lead others